import Vue from 'vue'
import Router from 'vue-router'
import movieList from './components/movie-list'
import movieCreate from './components/movie-create'
import movieEdit from './components/movie-edit'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'list',
            component: movieList,
            props: true
        },
        {
            path: '/movie/edit/:index',
            name: 'edit',
            component: movieEdit,
            props: true
        },
        {
            path: '/movie/add',
            name: 'add',
            component: movieCreate,
            props: true
        }
    ]
})
