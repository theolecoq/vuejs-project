import BootstrapVue from "bootstrap-vue";

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Vue from 'vue'
import router from './routes.js'

Vue.use(BootstrapVue);

new Vue({
    el: '#app',
    router,
    data: {
        movies: [
            {
                title: "HP 1",
                date: "2001",
                creator: {
                    name: "Chris Columbus",
                    nationality: "American",
                    birthDate: "10/09/1958"
                },
                synopsis: "Orphelin, le jeune Harry Potter peut enfin quitter ses tyranniques oncle et tante Dursley lorsqu'un curieux messager lui révèle qu'il est un sorcier. À 11 ans, Harry va enfin pouvoir intégrer la légendaire école de sorcellerie de Poudlard, y trouver une famille digne de ce nom et des amis, développer ses dons, et préparer son glorieux avenir.",
                type: "science fiction",
                rating: 5
            },
            {
                title: "HP 2",
                date: "2002",
                creator: {
                    name: "Chris Columbus",
                    nationality: "American",
                    birthDate: "10/09/1958"
                },
                synopsis: "L'elfe Dobby a bien tenté d'empêcher Harry de retourner à l'École des Sorciers, frappée d'une terrible malédiction, mais Harry n'est pas près de laisser choir ses amis. Après une fugue et une rentrée scolaire plutôt chaotique, voici notre valeureux sorcier intégré à Poudlard. Les forces maléfiques n'ont qu'à bien se tenir.",
                type: "science fiction",
                rating: 5
            },
            {
                title: "HP 3",
                date: "2003",
                creator: {
                    name: "Alfonso Cuaron",
                    nationality: "Mexican",
                    birthDate: "28/11/1961"
                },
                synopsis: "Sirius Black, un dangereux sorcier criminel, s'échappe de la sombre prison d'Azkaban avec un seul et unique but : se venger d'Harry Potter, entré avec ses amis Ron et Hermione en troisième année à l'école de sorcellerie de Poudlard, où ils auront aussi à faire avec les terrifiants Détraqueurs.",
                type: "science fiction",
                rating: 5
            },
            {
                title: "HP 4",
                date: "2004",
                creator: {
                    name: "Mike  Newell",
                    nationality: "British",
                    birthDate: "28/03/1942"
                },
                synopsis: "La quatrième année à l'école de Poudlard est marquée par le Tournoi des trois sorciers. Les participants sont choisis par la fameuse coupe de feu qui est à l'origine d'un scandale. Elle sélectionne Harry Potter alors qu'il n'a pas l'âge légal requis !",
                type: "science fiction",
                rating: 5
            },
            {
                title: "HP 5",
                date: "2005",
                creator: {
                    name: "David Yates",
                    nationality: "British",
                    birthDate: "30/10/1963"
                },
                synopsis: "Alors qu'il entame sa cinquième année d'études à Poudlard, Harry Potter découvre que la communauté des sorciers ne semble pas croire au retour de Voldemort, convaincue par une campagne de désinformation orchestrée par le Ministre de la Magie Cornelius Fudge.",
                type: "science fiction",
                rating: 5
            },
            {
                title: "HP 6",
                date: "2006",
                creator: {
                    name: "David Yates",
                    nationality: "British",
                    birthDate: "30/10/1963"
                },
                synopsis: "L'étau démoniaque de Voldemort se resserre sur l'univers des `Moldus' et le monde de la sorcellerie. Poudlard a cessé d'être un havre de paix, le danger rode au coeur du château. Cependant, Dumbledore est plus décidé que jamais à préparer Harry à son combat final, désormais imminent.",
                type: "science fiction",
                rating: 5
            },
            {
                title: "HP 7",
                date: "2007",
                creator: {
                    name: "David Yates",
                    nationality: "British",
                    birthDate: "30/10/1963"
                },
                synopsis: "Le dénouement des aventures du jeune sorcier, commencées sept éditions plus tôt, approche. Plus terrifiant et puissant que jamais, Voldemort, accompagné de ses fidèles servants, les Mangemorts, contrôle quasiment l'ensemble du monde des sorciers. Pour le vaincre, Harry, Ron et Hermione n'ont d'autre choix que de détruire les Horcruxes, garants de l'immortalité du Seigneur des Ténèbres. Seulement, leur mission n'est pas sans risques.",
                type: "science fiction",
                rating: 5
            }
        ]
    }
})